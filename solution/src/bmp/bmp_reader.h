//
// Created by viacheslav on 12.11.22.
//

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_BMP_READER_H
#define ASSIGNMENT_3_IMAGE_ROTATION_BMP_READER_H

#include "../image/image.h"
#include "../io/status.h"
#include <stdio.h>

enum read_status from_bmp(FILE **in, struct image *img);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_BMP_READER_H

