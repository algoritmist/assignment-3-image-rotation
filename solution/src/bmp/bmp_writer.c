//
// Created by viacheslav on 11.11.22.
//

#include "bmp_writer.h"
#include "bmp.h"

#define FORMAT_TYPE 0x4d42
#define RESERVED 0
#define INFO_HEADER_SIZE 40
#define PLANES_NUM 1
#define COMPRESSION_TYPE 0
#define H_RESOLUTION 2835
#define V_RESOLUTION 2835
#define BIT_COUNT 24
#define COLOR_USED 0
#define COLOR_IMPORTANT 0

/*static const struct bmp_header DEFAULT_HEADER = {
        .bfType = FORMAT_TYPE, // signature
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = INFO_HEADER_SIZE,
        .biPlanes = PLANES_NUM,
        .biBitCount = BIT_COUNT,
        .biCompression = COMPRESSION_TYPE,
        .biXPelsPerMeter = H_RESOLUTION,
        .biYPelsPerMeter = V_RESOLUTION,
        .biClrUsed = (1 << BIT_COUNT),
        .biClrImportant = 0,
};*/

static enum write_status write_header(FILE **out, struct bmp_header *const header) {
    if (fwrite(header, sizeof(struct bmp_header), 1, *out) != 1) {
        return WRITE_ERROR;
    }
    return WRITE_OK;
}

void init_header(struct bmp_header *header, struct image *const img) {
    uint32_t bmp_header_size = sizeof(struct bmp_header);
    uint32_t bmp_image_size = get_full_size(img);
    *header = (struct bmp_header) {
            .bfType = FORMAT_TYPE,
            .bfileSize = bmp_header_size + bmp_image_size,
            .bfReserved = RESERVED,
            .bOffBits = bmp_header_size,
            .biSize = INFO_HEADER_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = PLANES_NUM,
            .biBitCount = BIT_COUNT,
            .biCompression = COMPRESSION_TYPE,
            .biSizeImage = bmp_image_size,
            .biXPelsPerMeter = H_RESOLUTION,
            .biYPelsPerMeter = V_RESOLUTION,
            .biClrUsed = COLOR_USED,
            .biClrImportant = COLOR_IMPORTANT
    };

}


static enum write_status write_image(FILE **out, struct image *const img) {
    size_t width = img->width;
    uint8_t padding = get_padding(width);

    for (size_t row = 0; row < img->height; ++row) {
        size_t pixels_read = fwrite(img->data + row * width,
                                    sizeof(struct pixel),
                                    width, *out);
        if (pixels_read != width) {
            return WRITE_ERROR;
        }
        uint64_t zero = 0;
        if (fwrite(&zero, 1, padding, *out) != padding) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}

enum write_status to_bmp(FILE **out, struct image *const img) {
    struct bmp_header header;
    init_header(&header, img);
    enum write_status header_status = write_header(out, &header);
    if (header_status != WRITE_OK) {
        return header_status;
    }
    return write_image(out, img);
}
