//
// Created by viacheslav on 25.10.22.
//

#include "bmp.h"

#define PADDING_SIZE 4


uint8_t get_padding(uint64_t width) {
    return (PADDING_SIZE - width % PADDING_SIZE * sizeof(struct pixel) % PADDING_SIZE) % PADDING_SIZE;
}





