//
// Created by viacheslav on 14.10.22.
//

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_STATUS_H
#define ASSIGNMENT_3_IMAGE_ROTATION_STATUS_H
enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    /* other errors  */
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
    /* other errors */
};

enum open_status {
    OPEN_OK = 0,
    OPEN_NO_SUCH_FILE,
    OPEN_NO_PERMISSIONS
};

enum close_status {
    CLOSE_OK = 0,
    FAILED_TO_CLOSE
};
#endif //ASSIGNMENT_3_IMAGE_ROTATION_STATUS_H
