//
// Created by viacheslav on 25.10.22.
//

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_WRITER_H
#define ASSIGNMENT_3_IMAGE_ROTATION_WRITER_H

#include "status.h"
#include <stdio.h>

//Abstraction over file formats
typedef enum write_status (*fmt_writer)(FILE **, struct image*);

enum write_status write_image(fmt_writer writer, FILE **out, struct image *img);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_WRITER_H
