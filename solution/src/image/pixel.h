//
// Created by viacheslav on 14.10.22.
//

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_PIXEL_H

#include <inttypes.h>


struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};
#define ASSIGNMENT_3_IMAGE_ROTATION_PIXEL_H

#endif //ASSIGNMENT_3_IMAGE_ROTATION_PIXEL_H


