//
// Created by viacheslav on 25.10.22.
//
#include "image.h"


int alloc_image(struct image *const image, size_t width, size_t height) {
    struct pixel *pixels = malloc(sizeof(struct pixel) * width * height);
    if (!pixels) {
        return 0;
    }
    *image = (struct image) {.width = width, .height = height, .data = pixels};
    return 1;
}

void free_img(struct image *img) {
    free(img->data);
}

struct pixel get_pixel(struct image *const src, size_t row, size_t col) {
    return src->data[src->width * row + col];
}

void set_pixel(struct image *const dest, size_t row, size_t col, struct pixel value) {
    dest->data[dest->width * row + col] = value;
}

size_t get_size(struct image *const img) {
    return img->width * img->height;
}

size_t get_full_size(struct image *const img) {
    return get_size(img) * sizeof(struct pixel);
}
