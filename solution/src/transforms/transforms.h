//
// Created by viacheslav on 14.10.22.
//

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_TRANSFORMS_H
#define ASSIGNMENT_3_IMAGE_ROTATION_TRANSFORMS_H

#include "../image/image.h"

int rotate(struct image *source, struct image *dest);

int alloc_and_rotate(struct image *source, struct image *dest);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_TRANSFORMS_H


