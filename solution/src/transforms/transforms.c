//
// Created by viacheslav on 26.10.22.
//

#include "transforms.h"

int rotate(struct image *const source, struct image *const dest) {
    for (size_t row = 0; row < source->height; ++row) {
        for (size_t column = 0; column < source->width; ++column) {
            set_pixel(dest, column,
                      source->height - row - 1, get_pixel(source, row, column));
        }
    }
    return 1;
}

int alloc_and_rotate(struct image *const source, struct image *const dest) {
    int result = alloc_image(dest, source->height, source->width);
    return result && rotate(source, dest);
}
