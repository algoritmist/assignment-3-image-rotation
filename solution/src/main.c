//
// Created by viacheslav on 25.10.22.
//
#include "image/image.h"
#include "bmp/bmp_reader.h"
#include "bmp/bmp_writer.h"
#include "io/reader.h"
#include "io/writer.h"
#include "transforms/transforms.h"

#include <stdio.h>

int main(int argc, char **argv) {
    if (argc != 3) {
        fprintf(stderr, "Expected <input file> <output file>");
        return 1;
    }
    FILE *in = fopen(argv[1], "rb");

    struct image img = {0};

    enum read_status read_result = read_image(from_bmp, &in, &img);

    fclose(in);
    //free(in);


    if (read_result != READ_OK) {
        fprintf(stderr, "Failed to read image: %d", read_result);
        return 1;
    }

    struct image transformed_image;
    alloc_and_rotate(&img, &transformed_image);
    free_img(&img);
    FILE *out = fopen(argv[2], "wb");
    enum write_status write_result = write_image(to_bmp, &out, &transformed_image);
    fclose(out);
    free_img(&transformed_image);
    //free(out);
    //free_img(&img);
    if (write_result != WRITE_OK) {
        fprintf(stderr, "Failed to write image: %d", write_result);
        return 1;
    }
    printf("Successfully writed image");
    return 0;
}
